Project Bootstrap instructions for Linux:
=========================================

  In order to compile the source code, you need these packages on your machnie:

    * make >= 3.81-8.3
    * libglew-dev >= 1.10.0-3
    * clang >= 3.3-21
    * libsdl2-dev >= 2.0.2
    * libsdl2-image-dev >= 2.0.0
    * libsdl2-mixer-dev >= 2.0.0
    * libglm-dev >= 0.9.5.2-1

  Then the only thing you have to do is run the command: `make`.\n
  Start the application by invoking: `./indit`

Testing:
========

  Libraries:
  ----------

    * libgtest-dev >= 1.6 (or equivalent library built customly for -lgtest -lgtest_main)
    * gmock >= 1.7

  To run the unit tests, issue the command: `make check`.

Code coverage:
==============

  Libraries:
  ----------

    * lcov >= 1.10

  To run the test coverage, issue the command: `make coverage`.
