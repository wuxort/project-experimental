#include "CnCApp.h"
#include "Engine/Utils.h"

#include <math.h>
#include <vector>

CnCApp :: CnCApp(void) {}
CnCApp :: ~CnCApp(void) {
    delete textures;
    delete shaders;
    delete meshes;
    delete sound;
    delete plain;
    delete tank;
}

void CnCApp :: setGLSettings() {
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
}

void CnCApp :: setPerspective() {
    float screen_width = 800;
    float screen_height = 600;

    shaderAttributes.projection = glm::perspective( 45.0f, screen_width/screen_height, 0.1f, 1000.0f );
}

bool CnCApp :: Init() {
    glClearColor(0.125f, 0.25f, 0.5f, 1.0f);
    hitCount = 0;
    setGLSettings();
    shaders = new Shaders();
    setPerspective();
    textures = new Textures();
    meshes = new Meshes();
    sound = new Sound();
    sound -> load("Assets/sounds/Human2.mp3");
    sound -> play();

    plain = new BaseObject(meshes -> getMesh("rectangle"), textures -> getTexture("grass"), shaders -> getShader("basic"));
    plain -> setOrientation(glm::vec3(-90,0,0));
    plain -> setScale(glm::vec3(100,100,100));

    tank = new BaseObject(meshes -> getMesh("tank"), textures -> getTexture("tank"), shaders -> getShader("basic"));
    whelp = new BaseObject(meshes -> getMesh("whelp"), textures -> getTexture("whelp"), shaders -> getShader("basic"));
    whelp -> setPosition(glm::vec3(0,1,0));
    whelp -> setScale(glm::vec3(1.5, 1.5, 1.5));

    return true;
}

void CnCApp :: Update() {
    plain -> Update();
    tank -> setMatrix();
    whelp -> Update();

    for (int i = 0; i < bullets.size(); i++){
        bullets[i] -> moveForward();
        bullets[i] -> setMatrix();
        ticks[i]++;
        if(ticks[i] > 500){
            ticks.erase(ticks.begin()+i);
            bullets.erase(bullets.begin() + i);
        }
    }

    if(hitCount<100)
        for (int i = 0; i < bullets.size(); i++)
            if (getDistance(bullets[i] -> getPosition(), whelp -> getPosition()) < 5.0) {
                hitCount++;
                ticks.erase(ticks.begin()+i);
                bullets.erase(bullets.begin() + i);
                std::cout << "Hit: " << hitCount << std::endl;
            }

    shaderAttributes.view = glm::translate<float>(glm::vec3(0.0, -14.0, -40.0)) *
                            glm::rotate<float>(180, glm::vec3(0, 1, 0)) *
                            glm::inverse(tank->getModelMatrix());

}

void CnCApp :: Render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    plain -> draw(shaderAttributes);

    tank -> draw(shaderAttributes);

    if(hitCount<100)
        whelp -> draw(shaderAttributes);

    for(int i = 0; i < bullets.size(); i++)
        bullets[i] -> draw(shaderAttributes);
}

void CnCApp :: createBullet(float x, float y, float alpha) {
    BaseObject* obj = new BaseObject(meshes -> getMesh("sphere"), textures -> getTexture("fire"), shaders -> getShader("basic"));
    obj -> setPosition(glm::vec3(tank -> getPosition().x, tank-> getPosition().y + 1, tank -> getPosition().z));
    obj -> setOrientation(tank -> getOrientation());
    obj -> setIrany(tank->getIrany());
    bullets.push_back(obj);
    ticks.push_back(0);
}

void CnCApp :: KeyboardForward() { tank -> moveForward(); }
void CnCApp :: KeyboardBackward() { tank -> moveBackward(); }
void CnCApp :: KeyboardLeft() {
    tank -> rotateLeft();
}
void CnCApp :: KeyboardRight() { tank -> rotateRight(); }
void CnCApp :: KeyboardShoot() { createBullet(tank ->getPosition().x, tank -> getPosition().z, 0); }

void CnCApp :: MouseMove(SDL_MouseMotionEvent& mouse){}
void CnCApp :: MouseDown(SDL_MouseButtonEvent& mouse){}
void CnCApp :: MouseUp(SDL_MouseButtonEvent& mouse){}
void CnCApp :: MouseWheel(SDL_MouseWheelEvent& wheel){}

void CnCApp :: Resize(int width, int height) {
    glViewport(0, 0, width, height);
    shaderAttributes.projection = glm::perspective(45.0f, (float)width/(float)height, 0.01f, 100.0f);
}
