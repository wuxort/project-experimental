Install instructions:     {#mainpage}
=====================

  * For Windows see [Windows bootstrap instructions](docgen/windows.md)
  * For Linux see [Linux bootstrap instructions](docgen/linux.md)
