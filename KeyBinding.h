#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>

#if defined(_WIN32) || defined(_WIN64)
    #include <SDL.h>
#else
    #include <SDL2/SDL.h>
#endif

#include "Engine/Utils.h"

class KeyBinding
{
public:
    KeyBinding(std::string filename);
    ~KeyBinding();

    void save(std::string);
    void modify(std::string, SDL_Keycode);
    SDL_Keycode getKeycode (std::string);

private:
    std::map<std::string, SDL_Keycode>* config;
    std::map<std::string, SDL_Keycode>* load(std::string);

    template <typename stream>
    void writeBindings ( stream& where );
};
