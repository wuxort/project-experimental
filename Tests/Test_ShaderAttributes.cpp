#include "../Engine/ShaderAttributes.h"
#include <gtest/gtest.h>
#include <iostream>

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

TEST(ShaderAttributes, use_struct) {
    ShaderAttributes shaderAttributes;
    shaderAttributes.view[0][0] = 1;
    shaderAttributes.projection[0][1] = 2;
    ASSERT_EQ(1, shaderAttributes.view[0][0]);
    ASSERT_EQ(2, shaderAttributes.projection[0][1]);
}
