#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../Engine/Sound.h"
#include <SDL2/SDL_mixer.h>

using namespace ::testing;

class SoundMock : public Sound {
 public:
    MOCK_METHOD2(Mix_PlayMusic, void(Mix_Music *music, int loops));
    MOCK_METHOD1(Mix_LoadMUS, Mix_Music*(const char* filename));
};


TEST(SoundTest, load) {
    std::stringstream buffer;
    std::streambuf *sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());

    SoundMock mock;

    mock.load("Assets/sounds/Human2.mp3");
    ASSERT_THAT(buffer.str(), testing::MatchesRegex(".*Loaded music....*\n"));
    ASSERT_EXIT(mock.load("nonexistent"), ::testing::ExitedWithCode(1), ".*");

    std::cout.rdbuf(sbuf);
}

TEST(SoundTest, play) {
    SoundMock mock;
    mock.load("Assets/sounds/Human2.mp3");

    std::stringstream buffer;
    std::streambuf *sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());

    mock.play();

    ASSERT_THAT(buffer.str(), testing::MatchesRegex(".*Playing music...\n"));

    std::cout.rdbuf(sbuf);
}

TEST(SoundTest, stop) {
    SoundMock mock;
    mock.load("Assets/sounds/Human2.mp3");

    std::stringstream buffer;
    std::streambuf *sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());

    mock.play();
    mock.stop();

    ASSERT_THAT(buffer.str(), testing::MatchesRegex(".*Stopping music...\n"));

    std::cout.rdbuf(sbuf);
}
