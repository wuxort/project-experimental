#include "../Engine/Vertex.h"
#include <gtest/gtest.h>
#include <iostream>

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

TEST(Vertex_h_Test, use_struct) {
    Vertex vertex;
    vertex.position = glm::vec3(1,2,3);
    vertex.normal = glm::vec3(4,5,6);
    vertex.texcoord = glm::vec2(1,1);
    ASSERT_EQ(glm::vec3(1,2,3), vertex.position);
    ASSERT_EQ(glm::vec3(4,5,6), vertex.normal);
    ASSERT_EQ(glm::vec2(1,1), vertex.texcoord);
}
