#include "../Engine/Utils.h"
#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <vector>

TEST(UtilsTest, readFile) {
    std::string expected_file_contents = "Something\nLine2\nAnother Line\n";
    ASSERT_EQ(expected_file_contents, readFile("Tests/TestData/simple.txt"));
    ASSERT_ANY_THROW(readFile("nonexistent.txt"));
}

TEST(UtilsTest, getDistance) {
    ASSERT_EQ(1, getDistance(glm::vec3(0,0,0), glm::vec3(1,1,1)));
}

TEST(UtilsTest, trimEnd) {
    ASSERT_EQ("some", trimEnd("something", 5));
}

TEST(UtilsTest, getFilenamesFromDirectory) {
    std::vector<std::string> expected_filenames;
    expected_filenames.push_back("basic.frag");
    expected_filenames.push_back("color.frag");
    std::vector<std::string> actual_filenames = getFilenamesFromDirectory("Assets/Shaders/Fragment");
    ASSERT_EQ(expected_filenames, actual_filenames);
    ASSERT_EXIT(getFilenamesFromDirectory("Nonexistent"), ::testing::ExitedWithCode(1), ".*");
}
