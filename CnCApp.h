#pragma once

#include <GL/glew.h>

#if defined(_WIN32) || defined(_WIN64)
    #include <SDL.h>
    #include <SDL_opengl.h>

    #include <glm.hpp>
    #include <gtc/matrix_transform.hpp>
    #include <gtx/transform2.hpp>
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_opengl.h>

    #include <glm/glm.hpp>
    #include <glm/gtc/matrix_transform.hpp>
    #include <glm/gtx/transform2.hpp>
#endif

#include <vector>

#include "Engine/Meshes.h"
#include "Engine/Textures.h"
#include "Engine/Shaders.h"
#include "Engine/BaseObject.h"
#include "Engine/ShaderAttributes.h"
#include "Engine/Sound.h"

class CnCApp
{
public:
    CnCApp(void);
    ~CnCApp(void);

    bool Init();
    void Update();
    void Render();

    void KeyboardForward();
    void KeyboardBackward();
    void KeyboardLeft();
    void KeyboardRight();
    void KeyboardShoot();
    void MouseMove(SDL_MouseMotionEvent&);
    void MouseDown(SDL_MouseButtonEvent&);
    void MouseUp(SDL_MouseButtonEvent&);
    void MouseWheel(SDL_MouseWheelEvent&);
    void Resize(int, int);
    void createBullet(float, float, float);

private:
    Textures* textures;
    Meshes* meshes;
    Shaders* shaders;
    Sound* sound;

    int hitCount;
    std::vector<BaseObject*> bullets;
    std::vector<int> ticks;
    BaseObject* plain;
    BaseObject* tank;
    BaseObject* whelp;
    ShaderAttributes shaderAttributes;

    void setGLSettings();
    void setPerspective();
};
