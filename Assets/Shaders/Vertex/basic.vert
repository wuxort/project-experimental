#version 130

in vec3 vs_in_position;
in vec3 vs_in_normal;
in vec2 vs_in_texture;

out vec3 vs_out_col;
out vec2 vs_out_tex0;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4( vs_in_position, 1 );
    vs_out_col  = vs_in_normal;
    vs_out_tex0 = vs_in_texture;
}
