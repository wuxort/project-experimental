#include <GL/glew.h>

#if defined(_WIN32) || defined(_WIN64)
    #include <SDL.h>
    #include <SDL_opengl.h>
    #include <tchar.h>
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_opengl.h>
#endif


#include <iostream>
#include <sstream>

#include "CnCApp.h"
#include "KeyBinding.h"
#include "Engine/Utils.h"

int fullscreen = 0;

void setSDLAttributes() {
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,         32);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,            8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,          8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,           8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,          8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,        1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,          24);
    // antialiasing - ha kell
    //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);
    //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);
}

int getGLVersion(int level) {
    int glVersion;
    glGetIntegerv(level, &glVersion);
    return glVersion;
}

bool checkVersion(const SDL_GLContext& context, SDL_Window* win) {
    bool endstate = true;
    int glVersion[2];
    glVersion[0] = getGLVersion(GL_MAJOR_VERSION);
    glVersion[1] = getGLVersion(GL_MINOR_VERSION);

    std::cout << "Running OpenGL " << glVersion[0] << "." << glVersion[1] << std::endl;
    if ( glVersion[0] == -1 && glVersion[1] == -1 )
    {
        SDL_GL_DeleteContext(context);
        SDL_DestroyWindow( win );

        std::cout << "[GL] getVersion()" << std::endl;

        SDL_Delay(2000);
        endstate = false;
    }

    return endstate;
}

SDL_Window* createWindow() {
    Uint32 windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;

    if (fullscreen == 1)
        windowFlags |= SDL_WINDOW_FULLSCREEN;

    float screen_width = 800;
    float screen_height = 600;


    return SDL_CreateWindow( "CnC!", 100, 100, screen_width, screen_height, windowFlags);
}

#if defined(_WIN32) || defined(_WIN64)
    int _tmain(int argc, _TCHAR* argv[])
#else
    int main( int argc, char* args[] )
#endif
{
    KeyBinding keyBinding("keys.cfg");

    Uint32 SDL_InitList = SDL_INIT_VIDEO |
                          SDL_INIT_AUDIO;

    if ( SDL_Init( SDL_InitList ) == -1 ) {
        std::cout << "[SDL] SDL_Init(): " << SDL_GetError() << std::endl;
        return 1;
    }

    setSDLAttributes();

    SDL_Window* win = createWindow();

    if (win == 0) {
        std::cout << "[SDL] SDL_CreateWindow(): " << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(win);
    if (context == 0) {
        std::cout << "[SDL] SDL_GLContext(): " << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_GL_SetSwapInterval(1);

    GLenum error = glewInit();
    if ( error != GLEW_OK ) {
        std::cout << "[GLEW] glewInit()!" << std::endl;
        return 1;
    }

    if (!checkVersion(context, win)) {
        std::cout << "[GL] Version error..." << std::endl;
        return 1;
    }

    bool quit = false;
    SDL_Event ev;

    CnCApp* app = new CnCApp();
    if (!app -> Init()) {
        SDL_DestroyWindow(win);
        std::cout << "[app.Init] Error initializing application!" << std::endl;
        return 1;
    }

    char win_title[255] = "Project Experimental";
    SDL_SetWindowTitle(win, win_title);

    while (!quit) {
        while ( SDL_PollEvent(&ev) ) {
            switch (ev.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                if ( ev.key.keysym.sym == SDLK_ESCAPE || ev.key.keysym.sym == keyBinding.getKeycode("quit"))
                    quit = true;
                break;
            case SDL_KEYUP:
                break;
            case SDL_MOUSEBUTTONDOWN:
                app -> MouseDown(ev.button);
                break;
            case SDL_MOUSEBUTTONUP:
                app -> MouseDown(ev.button);
                break;
            case SDL_MOUSEWHEEL:
                app -> MouseWheel(ev.wheel);
                break;
            case SDL_MOUSEMOTION:
                app -> MouseMove(ev.motion);
                break;
            case SDL_WINDOWEVENT:
                if ( ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED )
                    app -> Resize(ev.window.data1, ev.window.data2);
                break;
            }
        }

        SDL_PumpEvents();

        const Uint8 * keys = SDL_GetKeyboardState(NULL);

        if (keys[SDL_GetScancodeFromKey(keyBinding.getKeycode("camera_forward"))]) {
            app -> KeyboardForward();
        }
        if (keys[SDL_GetScancodeFromKey(keyBinding.getKeycode("camera_backward"))]) {
             app -> KeyboardBackward();
        }
        if (keys[SDL_GetScancodeFromKey(keyBinding.getKeycode("camera_left"))]) {
             app -> KeyboardLeft();
        }
        if (keys[SDL_GetScancodeFromKey(keyBinding.getKeycode("camera_right"))]) {
             app -> KeyboardRight();
        }
        if (keys[SDL_GetScancodeFromKey(keyBinding.getKeycode("shoot"))]) {
             app -> KeyboardShoot();
        }

        app -> Update();
        app -> Render();

        SDL_GL_SwapWindow(win);
    }

    delete app;

    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow( win );

    SDL_Quit();

    return 0;
}
