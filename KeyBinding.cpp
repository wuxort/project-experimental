#include "KeyBinding.h"

#include <algorithm>
#include <stdio.h>
#include <ctype.h>

KeyBinding :: KeyBinding(std::string filename) {
    config = new std::map<std::string, SDL_Keycode>;
    this -> config = load(filename);
    std::cout << "======[ Keybinds:        ]======\n";
    writeBindings(std::cout);
    std::cout << "======[ Keybinds loaded! ]======\n" << std::endl;
}

KeyBinding :: ~KeyBinding() {
    delete config;
}

SDL_Keycode KeyBinding :: getKeycode (std::string key) {
    try {
        if (!config->empty())
            return config->at(key);
    }
    catch (const std::out_of_range& oor) {
        std::cerr << "KeyBinding :: getKeyCode(): No such key: " << key << std::endl;
    }

    return SDLK_UNKNOWN;
}

std::map<std::string, SDL_Keycode>* KeyBinding :: load (std::string filename) {
    std::map<std::string, SDL_Keycode>* config = new std::map<std::string, SDL_Keycode>;
    config -> clear();

    std::string separator = "=";
    std::string sLine = "";
    std::ifstream configfile(filename.c_str());

    if (configfile) {
        while (std::getline(configfile, sLine)) {

            if (sLine[0] == '#')
                continue;

            sLine.erase(std::remove_if(sLine.begin(), sLine.end(), ::isspace), sLine.end());
            std::size_t pos = sLine.find("=");

            if (pos == std::string::npos)
                std::cerr << "KeyBinding :: load(): Config parse error!" << std::endl;
            else {
                std::string key = sLine.substr(0, pos);
                std::string value = sLine.substr(pos+1);
                config -> insert(std::pair<std::string, SDL_Keycode>(key, SDL_GetKeyFromName(value.c_str())));
            }
        }
    }
    return config;
}

template <typename stream>
void KeyBinding :: writeBindings ( stream& where ) {
        std::map<std::string, SDL_Keycode>::iterator it;
        for (it = config->begin(); it != config->end(); it++)
            where << it->first << " = " << SDL_GetKeyName(it->second) << std::endl;
}

void KeyBinding :: save (std::string filename) {
    std::fstream file(filename);
    if (file.is_open())
        writeBindings(file);
}

void KeyBinding :: modify (std::string key, SDL_Keycode value) {
    if (!config->empty()) {
        config->erase(key);
        config->insert(std::pair<std::string, SDL_Keycode>(key, value));
    }
}
