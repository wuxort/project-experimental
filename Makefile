OUTPUT_FILE=indit

CXXFLAGS=-std=c++11
LIBS=-lGL -lSDL2 -lGLEW -lSDL2_image -lGLU -lSDL2_mixer
CPPFILES=$(shell find . -name "*.cpp" | grep -v "Test*")
TESTFILES=$(shell find . -name "*.cpp" | grep -v main.cpp)

default:
	if [ -e $(OUTPUT_FILE) ]; then rm $(OUTPUT_FILE); fi
	clang++ $(CXXFLAGS) $(LIBS) $(CPPFILES) -o $(OUTPUT_FILE)

check:
	if [ -e run_tests ]; then rm run_tests; fi
	clang++ $(CXXFLAGS) $(LIBS) $(TESTFILES) -lpthread -lgtest -lgmock -lgtest_main -o run_tests
	./run_tests
	rm run_tests

coverage:
	if [ -e run_tests ]; then rm run_tests; fi
	g++ $(CXXFLAGS) $(LIBS) $(TESTFILES) -o run_tests -lpthread -lgtest -lgmock -lgtest_main -fprofile-arcs -ftest-coverage
	./run_tests
	rm run_tests
	lcov --capture --directory . --output-file coverage_all.info
	lcov --remove coverage_all.info '/usr/lib/*' '/usr/include/*' 'Tests*' -o coverage_project.info
	if [ -d coverage_html ]; then rm -r coverage_html; fi
	genhtml coverage_project.info --output-directory coverage_html
	rm -rf *.gcda
	rm -rf *.gcno
	rm -f coverage_all.info
	rm -f coverage_project.info

clean:
	rm -rf *.gcda
	rm -rf *.gcno
	if [ -e coverage.info ]; then rm coverage.info; fi
	if [ -e run_tests ]; then rm run_tests; fi
	if [ -e indit ]; then rm indit; fi
	if [ -d documentation ]; then rm -rf documentation; fi

docs:
	if [ -d documentation ]; then rm -r documentation; fi
	doxygen doxygen.conf
