#include <SDL2/SDL_mixer.h>
#include <iostream>

class Sound {
public:
    /**
    * Constructor for starting the sound engine.
    */
    Sound();
    /**
    * Destructor for deinitializing the sound engine.
    */
    ~Sound();

    /**
      @param filename String argument.

      Opens the given filename, and stores it in a Mix_Music variable. This doesn't start playing it.
    */
    void load(std::string filename);
    /**
      Plays an already loaded file. If there were no loaded files to play, exit the program.
    */
    void play();
    /**
    * Stops playing a file.
    */
    void stop();

private:

    Mix_Music *music;

    Uint16 format;
    int rate;
    int channels;
    int buffers;
};
