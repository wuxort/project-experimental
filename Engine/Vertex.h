#ifndef _VERTEX_H_
#define _VERTEX_H_

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texcoord;
};

#endif
