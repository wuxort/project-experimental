#include "Mesh.h"
#include <vector>
#include <string>

class Meshes
{
public:
    /**
    * Constructor for the Meshes.
    */
    Meshes();
    /**
    * Destructor for the Meshes.
    */
    ~Meshes();

    /**
    * @param name The name of the mesh to be given back.
    *
    * Gives back a Mesh object of the give name.
    */
    Mesh* getMesh(std::string name);
private:
    std::vector<Mesh*> meshes;
};
