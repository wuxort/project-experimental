#include "Shader.h"
#include <iostream>
#include "Utils.h"

Shader :: Shader() {}
Shader :: ~Shader() {
    glDeleteProgram( shaderID );
    log("Shader: "+shaderName+" deallocated!");
}

GLuint Shader :: getShaderID() { return shaderID; }
std::string Shader :: getShaderName() { return shaderName; }

void Shader :: loadShader(std::string name) {
    static const std::string assetsPath = "Assets/Shaders/";

    std::string vertexFilepath = (assetsPath+"Vertex/"+name+".vert");
    std::string fragmentFilepath = (assetsPath+"Fragment/"+name+".frag");

    GLuint vertexShader = readShader(GL_VERTEX_SHADER, vertexFilepath.c_str());
    GLuint fragmentShader = readShader(GL_FRAGMENT_SHADER, fragmentFilepath.c_str());

    GLuint shaderProgramID = glCreateProgram();

    glAttachShader(shaderProgramID, vertexShader);
    glAttachShader(shaderProgramID, fragmentShader);

    glBindAttribLocation( shaderProgramID, 0, "vs_in_position");
    glBindAttribLocation( shaderProgramID, 1, "vs_in_normal");
    glBindAttribLocation( shaderProgramID, 2, "vs_in_texture");

    glLinkProgram(shaderProgramID);

    assert( isLinkingCorrect(shaderProgramID) );

    glDeleteShader( vertexShader );
    glDeleteShader( fragmentShader );

    shaderID = shaderProgramID;
    shaderName = name;

    log("Loaded shader: "+shaderName);
}

GLuint Shader :: readShader(GLenum shaderType, const char* fileName) {
    GLuint loadedShader = glCreateShader( shaderType );

    if ( loadedShader == 0 ) {
        std::cerr << "glCreateShader(): " << fileName << std::endl;
        return 0;
    }

    std::string shaderCode = readFile(fileName);
    const char* sourcePointer = shaderCode.c_str();

    glShaderSource( loadedShader, 1, &sourcePointer, NULL );
    glCompileShader( loadedShader );

    assert( isCompileCorrect(loadedShader) );

    return loadedShader;
}

bool Shader :: isCompileCorrect(GLuint shaderProgramID) {
    GLint result = GL_FALSE;
    int infoLogLength;

    glGetShaderiv(shaderProgramID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if ( GL_FALSE == result ) {
        std::vector<char> ShaderErrorMessage(infoLogLength);
        glGetShaderInfoLog(shaderProgramID, infoLogLength, NULL, &ShaderErrorMessage[0]);
        std::cerr << "Shader error: " << &ShaderErrorMessage[0] << std::endl;
        return false;
    }
    else
        return true;
}

bool Shader :: isLinkingCorrect(GLuint shaderProgramID) {
    GLint infoLogLength = 0,
          result = GL_FALSE;

    glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &result);
    glGetProgramiv(shaderProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);

    if ( GL_FALSE == result ) {
        std::vector<char> ProgramErrorMessage( infoLogLength );
        glGetProgramInfoLog(shaderProgramID, infoLogLength, NULL, &ProgramErrorMessage[0]);
        std::cerr << &ProgramErrorMessage[0] << std::endl;
        return false;
    }
    else
        return true;
}
