#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <GL/glew.h>
#include <iostream>

#if defined(_WIN32) || defined(_WIN64)
    #include <SDL_image.h>
#else
    #include <SDL2/SDL_image.h>
#endif

class Texture
{
public:
    /**
    * @param name The name of the texture to be loaded.
    *
    * Constructor for a Texture. Allocate texture memory on GPU and load a texture in it from the file given.
    */
    Texture(std::string name);
    /**
    * Destructor for the Texture.
    */
    ~Texture();

    /**
    * Returns the name of the texture.
    */
    std::string getTextureName();
    /**
    * Returns the ID of the texture.
    */
    GLuint getTextureID();
private:
    GLuint textureID;
    std::string textureName;

    GLuint TextureFromFile(std::string filename);
};

#endif
