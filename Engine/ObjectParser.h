#pragma once

#include <fstream>
#include <vector>
#include <map>

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

#include "Mesh.h"

class ObjParser
{
public:
    /**
    * Constructor for the ObjParser.
    */
    ObjParser(void) : nIndexedVerts(0) {}

    /**
    * @param fileName The name of the file to be parsed.
    *
    * Parses up a given filename, setting the vertices and indices.
    */
    void parse(const char* fileName);
    /**
    * Returns a list of Vertices which had been parsed up.
    */
    std::vector<Vertex> getVertices();
    /**
    * Returns a list of Indices which had been parsed up.
    */
    std::vector<unsigned int> getIndices();

    enum Exception { EXC_FILENOTFOUND };
private:
    struct IndexedVert {
        int v, vt, vn;
        IndexedVert(int _v, int _vt, int _vn) : v(_v), vt(_vt), vn(_vn) {};
        bool operator<(const IndexedVert& rhs) const {
            return v<rhs.v || (v == rhs.v && (vt<rhs.vt || (vt == rhs.vt && vn<rhs.vn)));
        }
    };

    bool processLine();
    bool skipCommentLine();
    void skipLine();
    void addIndexedVertex(const IndexedVert& vertex);

    std::ifstream ifs;

    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;

    unsigned int nIndexedVerts;
    std::map<IndexedVert, unsigned int> vertexIndices;
};
