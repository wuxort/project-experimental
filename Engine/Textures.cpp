#include "Textures.h"
#include "Utils.h"

Textures :: Textures() {
    std::vector<std::string> files = getFilenamesFromDirectory("Assets/textures");
    for (unsigned long i = 0; i < files.size(); ++i) {
        Texture* a_texture = new Texture(files[i]);
        textures.push_back(a_texture);
    }
}

Textures :: ~Textures() {
    for (unsigned long i = 0; i < textures.size(); ++i)
        delete textures[i];
    log("All textures deallocated!");
}

GLuint Textures :: getTexture(std::string name) {
    GLuint textureID = -1;

    for (unsigned long i = 0; i < textures.size(); ++i)
        if (textures[i] -> getTextureName() == name)
            textureID = textures[i] -> getTextureID();
    return textureID;
}
