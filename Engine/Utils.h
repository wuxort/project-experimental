#ifndef __UTILS_H__
#define __UTILS_H__

#pragma once

#include <iostream>
#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

#include <vector>

std::string readFile(std::string filename);
int getDistance(glm::vec3 position1, glm::vec3 position2);
std::string trimEnd(std::string from, int count);
std::vector<std::string> getFilenamesFromDirectory(std::string directory);
void log(std::string text);

#endif
