#include "BaseObject.h"

BaseObject :: BaseObject(Mesh* mesh, GLuint texture, GLuint shader) {
    this -> mesh = mesh;
    this -> texture = texture;
    this -> shader = shader;
    irany = 0;
    position = glm::vec3(0,0,0);
    orientation = glm::vec3(0,0,0);
    scale = glm::vec3(1,1,1);
    getUniformLocationsFromGPU(shader);
}

BaseObject :: ~BaseObject() {}

void BaseObject :: getUniformLocationsFromGPU(GLuint shaderProgramID) {
    gpu_model = glGetUniformLocation( shaderProgramID, "model");
    gpu_view = glGetUniformLocation( shaderProgramID, "view");
    gpu_projection = glGetUniformLocation( shaderProgramID, "projection");
    gpu_texture = glGetUniformLocation( shaderProgramID, "texture" );
}

void BaseObject :: draw(ShaderAttributes shaderAttributes) {
    glUseProgram( shader );

    glUniformMatrix4fv( gpu_model, 1, GL_FALSE, &(modelMatrix[0][0]) );
    glUniformMatrix4fv( gpu_view, 1, GL_FALSE, &(shaderAttributes.view[0][0]) );
    glUniformMatrix4fv( gpu_projection, 1, GL_FALSE, &(shaderAttributes.projection[0][0]) );

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(gpu_texture, 0);

    glBindVertexArray(mesh -> getMeshID());
    glDrawElements(GL_TRIANGLES, mesh -> getIndicesSize(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glUseProgram( 0 );
}

void BaseObject :: Update() {
    recalcModelMatrix();
}

void BaseObject :: recalcModelMatrix() {
    modelMatrix = glm::rotate<float>(orientation.x, glm::vec3(1, 0, 0)) *
           glm::rotate<float>(orientation.y, glm::vec3(0, 1, 0)) *
           glm::rotate<float>(orientation.z, glm::vec3(0, 0, 1)) *
           glm::translate<float>(position) *
           glm::scale<float>(scale);
}

glm::mat4 BaseObject :: getModelMatrix() {
    return modelMatrix;
}

glm::vec3 BaseObject :: getPosition() {
    return position;
}

void BaseObject :: setPosition(glm::vec3 position) {
    this -> position = glm::vec3(position.x, position.y, position.z);
}

glm::vec3 BaseObject :: getOrientation() {
    return orientation;
}

void BaseObject :: setOrientation(glm::vec3 orientation) {
    this -> orientation = orientation;
}

void BaseObject :: setScale(glm::vec3 scale) {
    this -> scale = scale;
}

void BaseObject::setMatrix(){
    modelMatrix = glm::scale<float>(glm::vec3(0.1, 0.1, 0.1)) * glm::translate<float>(position) * glm::rotate<float>(irany,glm::vec3(0.0,1.0,0.0));
}

void BaseObject :: rotateLeft(){
    irany += 0.5;
}

void BaseObject :: rotateRight(){
    irany -= 0.5;
}

void BaseObject :: moveForward(){
    glm::vec4 ir = glm::rotate(irany,glm::vec3(0.0f,1.0f,0.0f)) * glm::vec4(0.0,0.0,1.0,1.0);
    ir*=0.5f;
    position.x += ir.x;
    position.y += ir.y;
    position.z += ir.z;
}

void BaseObject :: moveBackward(){
    glm::vec4 ir = glm::rotate(irany,glm::vec3(0.0f,1.0f,0.0f)) * glm::vec4(0.0,0.0,1.0,1.0);
    ir*=0.5f;
    position.x -= ir.x;
    position.y -= ir.y;
    position.z -= ir.z;
}

float BaseObject :: getIrany(){
    return irany;
}

void BaseObject :: setIrany(float i){
    this -> irany = i;
}
