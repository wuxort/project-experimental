#include "Shaders.h"
#include "Utils.h"
#include <string>

Shaders :: Shaders() {
    std::vector<std::string> files = getFilenamesFromDirectory("Assets/Shaders/Fragment");
    for (unsigned long i = 0; i < files.size(); ++i) {
        Shader* a_shader = new Shader();
        a_shader -> loadShader(trimEnd(files[i], 5));
        shaders.push_back(a_shader);
    }
}
Shaders :: ~Shaders() {
    for (unsigned long i = 0; i < shaders.size(); ++i)
        delete shaders[i];
    log("All shaders deallocated!");
}

GLuint Shaders :: getShader(std::string name) {
    GLuint shaderID = -1;

    for (unsigned long i = 0; i < shaders.size(); ++i)
        if (shaders[i] -> getShaderName() == name)
            shaderID = shaders[i] -> getShaderID();

    return shaderID;
}
