#include <fstream>
#include <string>
#include <dirent.h>
#include <cstring>
#include "Utils.h"

#if defined(_WIN32) || defined(_WIN64)
    #include <SDL.h>
#else
    #include <SDL2/SDL.h>
#endif

std::string readFile(std::string filename) {
    std::string file_source = "";
    std::ifstream fileStream(filename);

    if ( !fileStream.is_open() ) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return 0;
    }

    std::string line = "";
    while ( std::getline(fileStream, line) )
        file_source += line + "\n";

    fileStream.close();

    return file_source;
}

int getDistance(glm::vec3 position1, glm::vec3 position2) {
    return sqrt(pow(position2.x-position1.x, 2)+pow(position2.y-position1.y, 2)+pow(position2.z-position1.z, 2));
}

std::string trimEnd(std::string from, int count) {
    return from.substr(0, from.size() - count);
}

std::vector<std::string> getFilenamesFromDirectory(std::string directory) {
    DIR* dir;
    struct dirent* ent;
    std::vector<std::string> files;

    if ((dir = opendir( directory.c_str() )) != NULL) {
        while ((ent = readdir (dir)) != NULL)
            if((strcmp(ent -> d_name, ".") != 0) && (strcmp(ent -> d_name, "..") != 0))
                files.push_back(ent -> d_name);
        closedir (dir);
    }
    else {
        perror ("");
        exit(1);
    }

    return files;
}

void log(std::string text) {
    std::cout << "[" << SDL_GetTicks() / 1000.0 << " sec] " << text << std::endl;
}
