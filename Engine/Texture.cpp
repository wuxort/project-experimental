#include "Texture.h"
#include "Utils.h"
#include <string>

Texture :: Texture(std::string name) {
    static const std::string assetsPath = "Assets/textures/";
    std::string texturePath = assetsPath+name;


    textureName = trimEnd(name, 4);
    textureID = TextureFromFile(texturePath.c_str());
}

Texture :: ~Texture() {
    glDeleteTextures(1, &textureID);
    log("Texture: "+textureName+" deallocated!");
}

std::string Texture :: getTextureName() { return textureName; }
GLuint Texture :: getTextureID() { return textureID; }

GLuint Texture :: TextureFromFile(std::string filename) {
    int flags = IMG_INIT_JPG | IMG_INIT_PNG;
    int initted = IMG_Init(flags);
    int img_mode = 0;

    if( (initted&flags) != flags) {
        std::cerr << "[SDL_Image] Failed to init required jpg and png support!" << std::endl;
        std::cerr << "[SDL_Image] Cause: " << IMG_GetError() << std::endl;
        return 1;
    }

    SDL_Surface* loaded_img = IMG_Load(filename.c_str());

    if ( loaded_img == 0 ) {
        std::cerr << "[SDL_Image] Error loading image: " << filename << std::endl;
        return 1;
    }

    if (loaded_img->format->Rmask != 255) {
        if ( loaded_img->format->BytesPerPixel == 4 )
            img_mode = GL_BGRA;
        else
            img_mode = GL_BGR;
    } else {
        if ( loaded_img->format->BytesPerPixel == 4 )
            img_mode = GL_RGBA;
        else
            img_mode = GL_RGB;
    }

    GLuint tex;
    glGenTextures(1, &tex);

    glBindTexture(GL_TEXTURE_2D, tex);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, loaded_img->w, loaded_img->h, img_mode, GL_UNSIGNED_BYTE, loaded_img->pixels);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    SDL_FreeSurface( loaded_img );

    log("Loaded texture: "+textureName);

    return tex;
}
