#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "ShaderAttributes.h"

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
    #include <gtc/matrix_transform.hpp>
    #include <gtx/transform2.hpp>
#else
    #include <glm/glm.hpp>
    #include <glm/gtc/matrix_transform.hpp>
    #include <glm/gtx/transform2.hpp>
#endif


class BaseObject {
public:
    /**
    * @param mesh The mesh of the object.
    * @param texture The texture of the object.
    * @param shader The shader for the object.
    *
    * Constructor for a BaseObject.
    */
    BaseObject(Mesh* mesh, GLuint texture, GLuint shader);
    /**
    * Destructor for a BaseObject.
    */
    ~BaseObject();

    /**
    * @param shaderAttributes View, Projection matrices for drawing.
    *
    * Loads meshes on GPU, turns on textures and does the actual drawing of an object.
    */
    void draw(ShaderAttributes shaderAttributes);
    /**
    * Function for calculating not drawing related things.
    */
    void Update();

    /**
    * Returns with the object's model matrix.
    */
    glm::mat4 getModelMatrix();
    /**
    * Returns with the object's position.
    */
    glm::vec3 getPosition();

    float getIrany();
    void setIrany(float);

    /**
    * Returns with the object's orientation.
    */
    glm::vec3 getOrientation();
    /**
    * @param position Vec3 position coordinates.
    *
    * Sets the object's actual position.
    */
    void setPosition(glm::vec3 position);
    /**
    * @param orientation Vec3 orientation coordinates.
    *
    * Sets the orientation of the object.
    */
    void setOrientation(glm::vec3 orientation);
    /**
    * @param scale Vec3 for scaling coordinates.
    *
    * Sets the scaling of an object.
    */
    void setScale(glm::vec3 scale);
    /**
    * Calculates the model matrix.
    */
    void setMatrix();
    /**
    * Rotates the object left.
    */
    void rotateLeft();
    /**
    * Rotates the object right.
    */
    void rotateRight();
    /**
    * Moves the object forward.
    */
    void moveForward();
    /**
    * Moves the object backwards.
    */
    void moveBackward();

private:
    Mesh* mesh;
    GLuint texture;
    GLuint shader;
    glm::mat4x4 modelMatrix;
    float irany;

    glm::vec3 position;
    glm::vec3 orientation;
    glm::vec3 scale;

    GLuint gpu_model;
    GLuint gpu_view;
    GLuint gpu_projection;
    GLuint gpu_texture;

    void recalcModelMatrix();
    void getUniformLocationsFromGPU(GLuint shaderProgramID);
};
