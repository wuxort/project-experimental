#include "ObjectParser.h"
#include "Utils.h"

#include <string>

using namespace std;

std::vector<Vertex> ObjParser :: getVertices() {
    return vertices;
}

std::vector<unsigned int> ObjParser :: getIndices() {
    return indices;
}

void ObjParser :: parse(const char* fileName) {
    ifs.open(fileName, ios::in|ios::binary);
    if (!ifs)
        throw(EXC_FILENOTFOUND);

    while(skipCommentLine())
        if (false == processLine())
            break;

    ifs.close();
}

bool ObjParser :: processLine() {
    string line_id;
    float x, y, z;

    if (!(ifs >> line_id))
        return false;

    if ("v" == line_id) {
        ifs >> x >> y >> z;
        positions.push_back(glm::vec3(x, y, z));
    }
    else if ("vt" == line_id) {
        ifs >> x >> y;
        texcoords.push_back(glm::vec2(x, y));
    }
    else if ("vn" == line_id) {
        ifs >> x >> y >> z;
        if(!ifs.good()) {
            x = y = z = 0.0;
            ifs.clear();
            skipLine();
        }
        normals.push_back(glm::vec3(x, y, z));
    }
    else if("f" == line_id) {
        unsigned int iPosition = 0, iTexCoord = 0, iNormal = 0;

        for( unsigned int iFace = 0; iFace < 3; iFace++ ) {
            ifs >> iPosition;
            if( '/' == ifs.peek() ) {
                ifs.ignore();

                if( '/' != ifs.peek() )
                    ifs >> iTexCoord;

                if( '/' == ifs.peek() ) {
                    ifs.ignore();
                    ifs >> iNormal;
                }
            }
            addIndexedVertex(IndexedVert(iPosition-1, iTexCoord-1, iNormal-1));
        }
    }
    else
        skipLine();
    return true;
}

void ObjParser :: addIndexedVertex(const IndexedVert& vertex) {
    if (vertexIndices[vertex] == 0) {
        Vertex v;
        v.position = positions[vertex.v];

        if (vertex.vt != -1)
            v.texcoord = texcoords[vertex.vt];
        if (vertex.vn != -1)
            v.normal = normals[vertex.vn];

        vertices.push_back(v);
        indices.push_back(nIndexedVerts++);
        vertexIndices[vertex] = nIndexedVerts;
    } else {
        indices.push_back(vertexIndices[vertex]-1);
    }
}

bool ObjParser :: skipCommentLine() {
    char next;
    while( ifs >> std::skipws >> next ) {
        ifs.putback(next);
        if ('#' == next)
            skipLine();
        else
            return true;
    }
    return false;
}

void ObjParser :: skipLine() {
    char next;
    ifs >> std::noskipws;
    while( (ifs >> next) && ('\n' != next) );
}
