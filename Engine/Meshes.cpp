#include "Meshes.h"
#include "Utils.h"

Meshes :: Meshes() {
    std::vector<std::string> files = getFilenamesFromDirectory("Assets/models");
    for (unsigned long i = 0; i < files.size(); ++i) {
        Mesh* a_mesh = new Mesh(files[i]);
        meshes.push_back(a_mesh);
    }
}
Meshes :: ~Meshes() {
    for (unsigned long i = 0; i < meshes.size(); ++i)
        delete meshes[i];
    log("All meshes deallocated!");
}

Mesh* Meshes :: getMesh(std::string name) {
    Mesh* mesh = NULL;

    for (unsigned long i = 0; i < meshes.size(); ++i)
        if (meshes[i] -> getMeshName() == name)
            mesh = meshes[i];

    return mesh;
}
