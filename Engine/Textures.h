#include "Texture.h"
#include <vector>
#include <string>

class Textures
{
public:
    /**
    * Constructor for Textures. Loads all the textures form the Assets.
    */
    Textures();
    /**
    * Destructor for Textures.
    */
    ~Textures();

    /**
    * @param name The name of the texture to be given back.
    *
    * Returns with a texture of the given name.
    */
    GLuint getTexture(std::string name);
private:
    std::vector<Texture*> textures;
};
