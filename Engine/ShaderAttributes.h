#ifndef __SHADERATTRIBUTES_H__
#define __SHADERATTRIBUTES_H__

#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

struct ShaderAttributes
{
    glm::mat4x4 view;
	glm::mat4x4 projection;
};

#endif
