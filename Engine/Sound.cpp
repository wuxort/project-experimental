#include <iostream>
#include <assert.h>
#include "Sound.h"
#include "Utils.h"

Sound :: Sound () {
    rate = 22050;
    format = AUDIO_S16;
    channels = 2;
    buffers = 4096;

    if ( Mix_OpenAudio(rate, format, channels, buffers)) {
        std::cerr << "Unable to open audio engine!" << std::endl;
        exit(1);
    }
}

Sound :: ~Sound () {
    this -> stop();
    Mix_FreeMusic(music);
    Mix_CloseAudio();
    log("Audio engine closed.");
}

void Sound :: load (std::string filename) {
    music = Mix_LoadMUS(filename.c_str());
    if (music == NULL) {
        std::cerr << "Cannot load music file: " << filename << std::endl;
        exit(1);
    }
    log("Loaded music: "+filename);
}

void Sound :: play() {
    Mix_PlayMusic(music, 0);
    log("Playing music...");
}

void Sound :: stop() {
    Mix_HaltMusic();
    log("Stopping music...");
}
