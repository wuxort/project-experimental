#include <vector>
#include "Shader.h"
#include <string>

class Shaders
{
public:
    /**
    * Constructor for Shaders. Loads all the available Shaders from the Assets directory.
    */
    Shaders();
    /**
    * Destructor for Shaders.
    */
    ~Shaders();

    /**
    * @param name The shader to be queried.
    *
    * Returns a Shader of the given name.
    */
    GLuint getShader(std::string name);
private:
    std::vector<Shader*> shaders;
};
