#pragma once

#include <GL/glew.h>

#include <vector>
#if defined(_WIN32) || defined(_WIN64)
    #include <glm.hpp>
#else
    #include <glm/glm.hpp>
#endif

#include "Vertex.h"
#include <iostream>

class Mesh
{
public:
    /**
    * @param filename File to be loaded and parsed.
    *
    * Constructor for a Mesh object.
    */
    Mesh(std::string filename);
    /**
    * Destructor for the Mesh object.
    */
    ~Mesh(void);

    /**
    * Initializes the vertex and index buffers of the object.
    */
    void initBuffers();
    /**
    * Draws out the object.
    */
    void draw();

    /**
    * @param vertex A vertex.
    *
    * Adds a vertex to the object.
    */
    void addVertex(const Vertex& vertex);
    /**
    * @param index An index.
    *
    * Adds an index to the object.
    */
    void addIndex(unsigned int index);

    /**
    * @param vertices The vertices of the object.
    * @param size The size of the vertices array.
    *
    * Sets the object's vertices to the given parameters.
    */
    void setVertices(Vertex vertices[], GLuint size);
    /**
    * @param indices The indices of the object.
    * @param size The size of the indices array.
    *
    * Sets the indices of the object to the given parameters.
    */
    void setIndices(GLuint indices[], GLushort size);

    /**
    * Returns the Meshes name.
    */
    std::string getMeshName();
    /**
    * Returns the meshes ID.
    */
    GLuint getMeshID();
    /**
    * Returns the size of the indices array.
    */
    int getIndicesSize();
private:
    GLuint vertexArrayObjectID;
    GLuint vertexBufferObjectID;
    GLuint indexBufferObjectID;

    std::string meshName;
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;

    void initIndexBuffers();
    void initVertexBuffers();
};
