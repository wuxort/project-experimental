#ifndef __SHADER_H__
#define __SHADER_H__

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>

class Shader
{
public:
    /**
    * Constructor for making a shader.
    */
    Shader();
    /**
    * Destructor for shader.
    */
    ~Shader();

    /**
    * @param fileName Name of the file to be loaded.
    *
    * Loads a shader from a given file, then compiles it.
    */
    void loadShader(std::string fileName);

    /**
    * Gets a loaded shader's ID.
    */
    GLuint getShaderID();
    /**
    * Gives back a loaded shader's name.
    */
    std::string getShaderName();
private:
    GLuint shaderID;
    std::string shaderName;

    GLuint readShader(GLenum shaderType, const char* fileName);
    bool isCompileCorrect(GLuint shaderProgramID);
    bool isLinkingCorrect(GLuint shaderProgramID);
};

#endif
